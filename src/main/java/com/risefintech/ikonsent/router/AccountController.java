/**
 * 
 */
package com.risefintech.ikonsent.router;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.risefintech.ikonsent.model.domain.schema.GenericRequest;
import com.risefintech.ikonsent.model.domain.schema.GenericResponse;
import com.risefintech.ikonsent.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * @author abhinab
 *
 */
@RestController
@Slf4j
public class AccountController {

  @Autowired
  private AccountService accountService;

  @PostMapping("/v1/accounts")
  public Mono<GenericResponse> accounts(
      @RequestBody(required = true) Mono<GenericRequest> genericRequest) {
    if (log.isDebugEnabled()) {
      log.debug("Method : accounts");
    }

    GenericResponse response = GenericResponse.builder().build();

    return genericRequest.map(request -> accountService.processAccounts(request, response))
        .map(accounts -> {
          setResp(accounts, response);
          return response;
        });
  }

  @PostMapping("/v1/accounts/{accountId}")
  public Mono<GenericResponse> accountById(
      @RequestBody(required = true) Mono<GenericRequest> genericRequest,
      @PathVariable(value = "accountId", required = true) String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : accountById");
    }

    GenericResponse response = GenericResponse.builder().build();

    return genericRequest
        .map(request -> accountService.processAccountById(request, response, accountId))
        .map(account -> {
          setResp(account, response);
          return response;
        });
  }

  @PostMapping("/v1/accounts/{accountId}/product")
  public Mono<GenericResponse> accountProduct(
      @RequestBody(required = true) Mono<GenericRequest> genericRequest,
      @PathVariable(value = "accountId", required = true) String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : accountProduct");
    }

    GenericResponse response = GenericResponse.builder().build();

    return genericRequest
        .map(request -> accountService.processAccountProduct(request, response, accountId))
        .map(account -> {
          setResp(account, response);
          return response;
        });
  }

  @PostMapping("/v1/accounts/{accountId}/balances")
  public Mono<GenericResponse> accountBalances(
      @RequestBody(required = true) Mono<GenericRequest> genericRequest,
      @PathVariable(value = "accountId", required = true) String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : accountBalances");
    }

    GenericResponse response = GenericResponse.builder().build();

    return genericRequest
        .map(request -> accountService.processAccountBalances(request, response, accountId))
        .map(account -> {
          setResp(account, response);
          return response;
        });
  }

  @PostMapping("/v1/accounts/{accountId}/transactions")
  public Mono<GenericResponse> accountTransactions(
      @RequestBody(required = true) Mono<GenericRequest> genericRequest,
      @PathVariable(value = "accountId", required = true) String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : accountTransactions");
    }

    GenericResponse response = GenericResponse.builder().build();

    return genericRequest
        .map(request -> accountService.processAccountTransactions(request, response, accountId))
        .map(account -> {
          setResp(account, response);
          return response;
        });
  }

  @PostMapping("/v1/accounts/{accountId}/direct-debits")
  public Mono<GenericResponse> accountDirectDebits(
      @RequestBody(required = true) Mono<GenericRequest> genericRequest,
      @PathVariable(value = "accountId", required = true) String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : accountDirectDebits");
    }

    GenericResponse response = GenericResponse.builder().build();

    return genericRequest
        .map(request -> accountService.processAccountDirectDebits(request, response, accountId))
        .map(account -> {
          setResp(account, response);
          return response;
        });
  }

  @PostMapping("/v1/accounts/{accountId}/standing-orders")
  public Mono<GenericResponse> accountStandingOrders(
      @RequestBody(required = true) Mono<GenericRequest> genericRequest,
      @PathVariable(value = "accountId", required = true) String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : accountStandingOrders");
    }

    GenericResponse response = GenericResponse.builder().build();

    return genericRequest
        .map(request -> accountService.processAccountStandingOrders(request, response, accountId))
        .map(account -> {
          setResp(account, response);
          return response;
        });
  }

  @PostMapping("/v1/accounts/{accountId}/beneficiaries")
  public Mono<GenericResponse> accountBeneficiaries(
      @RequestBody(required = true) Mono<GenericRequest> genericRequest,
      @PathVariable(value = "accountId", required = true) String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : accountBeneficiaries");
    }

    GenericResponse response = GenericResponse.builder().build();

    return genericRequest
        .map(request -> accountService.processAccountBeneficiaries(request, response, accountId))
        .map(account -> {
          setResp(account, response);
          return response;
        });
  }

  private GenericResponse setResp(Optional<?> data, GenericResponse response) {
    if (data.isPresent()) {
      response.setSuccess(true);
      response.setData(data.get());
    }
    return response;
  }

}
