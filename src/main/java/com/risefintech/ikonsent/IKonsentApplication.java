package com.risefintech.ikonsent;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * 
 * @author abhinab
 *
 */
@SpringBootApplication
public class IKonsentApplication {

  public static void main(String[] args) {
    SpringApplication.run(IKonsentApplication.class, args);
  }

  @Bean
  CommandLineRunner run() {
    return args -> {
      System.out.println("|||   Application Started   |||");
    };
  }

}
