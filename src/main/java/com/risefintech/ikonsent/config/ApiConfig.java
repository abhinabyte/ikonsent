/**
 * 
 */
package com.risefintech.ikonsent.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import lombok.Data;

/**
 * @author abhinab
 *
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "api")
public class ApiConfig {

  private String accounts;
  private String account;
  private String product;
  private String balance;
  private String transaction;
  private String directDebit;
  private String standingOrder;
  private String beneficiary;

}
