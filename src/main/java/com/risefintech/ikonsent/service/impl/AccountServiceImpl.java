/**
 * 
 */
package com.risefintech.ikonsent.service.impl;

import org.springframework.stereotype.Service;
import com.risefintech.ikonsent.config.ApiConfig;
import com.risefintech.ikonsent.model.domain.resp.Account;
import com.risefintech.ikonsent.model.domain.resp.Accounts;
import com.risefintech.ikonsent.model.domain.resp.Balance;
import com.risefintech.ikonsent.model.domain.resp.Beneficiary;
import com.risefintech.ikonsent.model.domain.resp.DirectDebit;
import com.risefintech.ikonsent.model.domain.resp.Product;
import com.risefintech.ikonsent.model.domain.resp.StandingOrder;
import com.risefintech.ikonsent.model.domain.resp.Transaction;
import com.risefintech.ikonsent.model.domain.schema.Error;
import com.risefintech.ikonsent.model.domain.schema.GenericRequest;
import com.risefintech.ikonsent.model.domain.schema.GenericResponse;
import com.risefintech.ikonsent.service.AccountService;
import com.risefintech.ikonsent.util.CommonUtil;
import com.risefintech.ikonsent.util.rest.IKonsentRestClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.util.StringUtils.isEmpty;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author abhinab
 *
 */
@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

  private IKonsentRestClient iKonsentRestClient = IKonsentRestClient.getInstance();

  @Autowired
  private ApiConfig apiConfig;

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.risefintech.ikonsent.service.AccountService#processAccounts(com.risefintech.ikonsent.model.
   * domain.schema.GenericRequest, com.risefintech.ikonsent.model.domain.schema.GenericResponse)
   */
  @Override
  public Optional<Accounts> processAccounts(GenericRequest request, GenericResponse response) {
    if (log.isDebugEnabled()) {
      log.debug("Method : processAccounts");
    }

    validate(request, response);

    if (!CommonUtil.isEmpty(response.getErrors())) {
      return Optional.empty();
    }

    Accounts accounts = null;

    String url = apiConfig.getAccounts();

    try {
      accounts = iKonsentRestClient
          .getForEntity(url, request.getPspId(), request.getAuthorization(), Accounts.class)
          .getBody();
    } catch (Exception e) {
      log.error("Unable to connect to the API: {}", url, e);
      response.getErrors().add(createError("UTS", "Unable to connect to the API"));
      return Optional.empty();
    }

    return Optional.of(accounts);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.risefintech.ikonsent.service.AccountService#processAccountById(com.risefintech.ikonsent.
   * model.domain.schema.GenericRequest,
   * com.risefintech.ikonsent.model.domain.schema.GenericResponse, java.lang.String)
   */
  @Override
  public Optional<List<Account>> processAccountById(GenericRequest request,
      GenericResponse response, String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : processAccountById");
    }

    validate(request, response);

    if (!CommonUtil.isEmpty(response.getErrors())) {
      return Optional.empty();
    }

    List<Account> accountData = new ArrayList<>();

    String url = String.format(apiConfig.getAccount(), accountId);

    try {
      Accounts accounts = iKonsentRestClient
          .getForEntity(url, request.getPspId(), request.getAuthorization(), Accounts.class)
          .getBody();

      if (!isEmpty(accounts) && !isEmpty(accounts.getData())) {
        accountData = accounts.getData().getAccounts();
      }
    } catch (Exception e) {
      log.error("Unable to connect to the API: {}", url, e);
      response.getErrors().add(createError("UTS", "Unable to connect to the API"));
      return Optional.empty();
    }

    return Optional.of(accountData);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.risefintech.ikonsent.service.AccountService#processAccountProduct(com.risefintech.ikonsent.
   * model.domain.schema.GenericRequest,
   * com.risefintech.ikonsent.model.domain.schema.GenericResponse, java.lang.String)
   */
  @Override
  public Optional<List<Product>> processAccountProduct(GenericRequest request,
      GenericResponse response, String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : processAccountProduct");
    }

    validate(request, response);

    if (!CommonUtil.isEmpty(response.getErrors())) {
      return Optional.empty();
    }

    List<Product> products = new ArrayList<>();

    String url = String.format(apiConfig.getProduct(), accountId);

    try {
      Accounts accounts = iKonsentRestClient
          .getForEntity(url, request.getPspId(), request.getAuthorization(), Accounts.class)
          .getBody();

      if (!isEmpty(accounts) && !isEmpty(accounts.getData())) {
        products = accounts.getData().getProducts();
      }
    } catch (Exception e) {
      log.error("Unable to connect to the API: {}", url, e);
      response.getErrors().add(createError("UTS", "Unable to connect to the API"));
      return Optional.empty();
    }

    return Optional.of(products);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.risefintech.ikonsent.service.AccountService#processAccountBalances(com.risefintech.ikonsent
   * .model.domain.schema.GenericRequest,
   * com.risefintech.ikonsent.model.domain.schema.GenericResponse, java.lang.String)
   */
  @Override
  public Optional<List<Balance>> processAccountBalances(GenericRequest request,
      GenericResponse response, String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : processAccountBalances");
    }

    validate(request, response);

    if (!CommonUtil.isEmpty(response.getErrors())) {
      return Optional.empty();
    }

    List<Balance> balances = new ArrayList<>();

    String url = String.format(apiConfig.getBalance(), accountId);

    try {
      Accounts accounts = iKonsentRestClient
          .getForEntity(url, request.getPspId(), request.getAuthorization(), Accounts.class)
          .getBody();

      if (!isEmpty(accounts) && !isEmpty(accounts.getData())) {
        balances = accounts.getData().getBalances();
      }
    } catch (Exception e) {
      log.error("Unable to connect to the API: {}", url, e);
      response.getErrors().add(createError("UTS", "Unable to connect to the API"));
      return Optional.empty();
    }

    return Optional.of(balances);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.risefintech.ikonsent.service.AccountService#processAccountTransactions(com.risefintech.
   * ikonsent.model.domain.schema.GenericRequest,
   * com.risefintech.ikonsent.model.domain.schema.GenericResponse, java.lang.String)
   */
  @Override
  public Optional<List<Transaction>> processAccountTransactions(GenericRequest request,
      GenericResponse response, String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : processAccountTransactions");
    }

    validate(request, response);

    if (!CommonUtil.isEmpty(response.getErrors())) {
      return Optional.empty();
    }

    List<Transaction> transactions = new ArrayList<>();

    String url = String.format(apiConfig.getTransaction(), accountId);

    try {
      Accounts accounts = iKonsentRestClient
          .getForEntity(url, request.getPspId(), request.getAuthorization(), Accounts.class)
          .getBody();

      if (!isEmpty(accounts) && !isEmpty(accounts.getData())) {
        transactions = accounts.getData().getTransactions();
      }
    } catch (Exception e) {
      log.error("Unable to connect to the API: {}", url, e);
      response.getErrors().add(createError("UTS", "Unable to connect to the API"));
      return Optional.empty();
    }

    return Optional.of(transactions);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.risefintech.ikonsent.service.AccountService#processAccountDirectDebits(com.risefintech.
   * ikonsent.model.domain.schema.GenericRequest,
   * com.risefintech.ikonsent.model.domain.schema.GenericResponse, java.lang.String)
   */
  @Override
  public Optional<List<DirectDebit>> processAccountDirectDebits(GenericRequest request,
      GenericResponse response, String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : processAccountDirectDebits");
    }

    validate(request, response);

    if (!CommonUtil.isEmpty(response.getErrors())) {
      return Optional.empty();
    }

    List<DirectDebit> directDebits = new ArrayList<>();

    String url = String.format(apiConfig.getDirectDebit(), accountId);

    try {
      Accounts accounts = iKonsentRestClient
          .getForEntity(url, request.getPspId(), request.getAuthorization(), Accounts.class)
          .getBody();

      if (!isEmpty(accounts) && !isEmpty(accounts.getData())) {
        directDebits = accounts.getData().getDirectDebits();
      }
    } catch (Exception e) {
      log.error("Unable to connect to the API: {}", url, e);
      response.getErrors().add(createError("UTS", "Unable to connect to the API"));
      return Optional.empty();
    }

    return Optional.of(directDebits);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.risefintech.ikonsent.service.AccountService#processAccountStandingOrders(com.risefintech.
   * ikonsent.model.domain.schema.GenericRequest,
   * com.risefintech.ikonsent.model.domain.schema.GenericResponse, java.lang.String)
   */
  @Override
  public Optional<List<StandingOrder>> processAccountStandingOrders(GenericRequest request,
      GenericResponse response, String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : processAccountStandingOrders");
    }

    validate(request, response);

    if (!CommonUtil.isEmpty(response.getErrors())) {
      return Optional.empty();
    }

    List<StandingOrder> standingOrders = new ArrayList<>();

    String url = String.format(apiConfig.getStandingOrder(), accountId);

    try {
      Accounts accounts = iKonsentRestClient
          .getForEntity(url, request.getPspId(), request.getAuthorization(), Accounts.class)
          .getBody();

      if (!isEmpty(accounts) && !isEmpty(accounts.getData())) {
        standingOrders = accounts.getData().getStandingOrders();
      }
    } catch (Exception e) {
      log.error("Unable to connect to the API: {}", url, e);
      response.getErrors().add(createError("UTS", "Unable to connect to the API"));
      return Optional.empty();
    }

    return Optional.of(standingOrders);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.risefintech.ikonsent.service.AccountService#processAccountBeneficiaries(com.risefintech.
   * ikonsent.model.domain.schema.GenericRequest,
   * com.risefintech.ikonsent.model.domain.schema.GenericResponse, java.lang.String)
   */
  @Override
  public Optional<List<Beneficiary>> processAccountBeneficiaries(GenericRequest request,
      GenericResponse response, String accountId) {
    if (log.isDebugEnabled()) {
      log.debug("Method : processAccountBeneficiaries");
    }

    validate(request, response);

    if (!CommonUtil.isEmpty(response.getErrors())) {
      return Optional.empty();
    }

    List<Beneficiary> beneficiaries = new ArrayList<>();

    String url = String.format(apiConfig.getBeneficiary(), accountId);

    try {
      Accounts accounts = iKonsentRestClient
          .getForEntity(url, request.getPspId(), request.getAuthorization(), Accounts.class)
          .getBody();

      if (!isEmpty(accounts) && !isEmpty(accounts.getData())) {
        beneficiaries = accounts.getData().getBeneficiaries();
      }
    } catch (Exception e) {
      log.error("Unable to connect to the API: {}", url, e);
      response.getErrors().add(createError("UTS", "Unable to connect to the API"));
      return Optional.empty();
    }

    return Optional.of(beneficiaries);
  }

  private void validate(GenericRequest request, GenericResponse response) {
    if (isEmpty(request) || isEmpty(request.getPspId()) || isEmpty(request.getAuthorization())) {
      response.getErrors().add(createError("IPR", "Invalid payload received"));
    }
  }

  private Error createError(String code, String desc) {
    return Error.builder().code(code).desc(desc).build();
  }

}
