/**
 * 
 */
package com.risefintech.ikonsent.service;

import java.util.List;
import java.util.Optional;
import com.risefintech.ikonsent.model.domain.resp.Account;
import com.risefintech.ikonsent.model.domain.resp.Accounts;
import com.risefintech.ikonsent.model.domain.resp.Balance;
import com.risefintech.ikonsent.model.domain.resp.Beneficiary;
import com.risefintech.ikonsent.model.domain.resp.DirectDebit;
import com.risefintech.ikonsent.model.domain.resp.Product;
import com.risefintech.ikonsent.model.domain.resp.StandingOrder;
import com.risefintech.ikonsent.model.domain.resp.Transaction;
import com.risefintech.ikonsent.model.domain.schema.GenericRequest;
import com.risefintech.ikonsent.model.domain.schema.GenericResponse;

/**
 * @author abhinab
 *
 */
public interface AccountService {

  /**
   * 
   * Retrieve account information for a consent identifier
   */
  Optional<Accounts> processAccounts(GenericRequest request, GenericResponse response);

  /**
   * 
   * Retrieve account information for an account identifier
   */
  Optional<List<Account>> processAccountById(GenericRequest request, GenericResponse response,
      String accountId);

  /**
   * 
   * Retrieve product information for an account identifier
   */
  Optional<List<Product>> processAccountProduct(GenericRequest request, GenericResponse response,
      String accountId);

  /**
   * 
   * Retrieve balance information for an account identifier
   */
  Optional<List<Balance>> processAccountBalances(GenericRequest request, GenericResponse response,
      String accountId);

  /**
   * 
   * Retrieve pending and booked transaction information for an account identifier
   */
  Optional<List<Transaction>> processAccountTransactions(GenericRequest request,
      GenericResponse response, String accountId);

  /**
   * 
   * Retrieve direct debit information for an account identifier
   */
  Optional<List<DirectDebit>> processAccountDirectDebits(GenericRequest request,
      GenericResponse response, String accountId);

  /**
   * 
   * Retrieve standing order information for an account identifier
   */
  Optional<List<StandingOrder>> processAccountStandingOrders(GenericRequest request,
      GenericResponse response, String accountId);

  /**
   * 
   * Retrieve beneficiaries information for an account identifier
   */
  Optional<List<Beneficiary>> processAccountBeneficiaries(GenericRequest request,
      GenericResponse response, String accountId);

}
