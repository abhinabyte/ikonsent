/**
 * 
 */
package com.risefintech.ikonsent.util;

import java.util.Collection;
import lombok.experimental.UtilityClass;

/**
 * @author abhinab
 *
 */
@UtilityClass
public class CommonUtil {

  public static boolean isEmpty(Collection<?> collection) {
    return collection == null || collection.isEmpty();
  }

}
