/**
 * 
 */
package com.risefintech.ikonsent.util.rest;

import java.io.IOException;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * @author abhinab
 *
 */
public class HeaderRequestInterceptor implements ClientHttpRequestInterceptor {

  private String headerName;
  private String headerValue;

  public HeaderRequestInterceptor(String headerName, String headerValue) {
    this.headerName = headerName;
    this.headerValue = headerValue;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.springframework.http.client.ClientHttpRequestInterceptor#intercept(
   * org.springframework.http.HttpRequest, byte[],
   * org.springframework.http.client.ClientHttpRequestExecution)
   */
  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body,
      ClientHttpRequestExecution execution) throws IOException {
    request.getHeaders().set(headerName, headerValue);
    return execution.execute(request, body);
  }

}
