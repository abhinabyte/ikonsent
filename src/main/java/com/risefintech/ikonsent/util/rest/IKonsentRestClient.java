/**
 * 
 */
package com.risefintech.ikonsent.util.rest;

import java.util.Arrays;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author abhinab
 *
 */
public class IKonsentRestClient {

  private static final String HEADER_FINANCIAL_ID = "x-fapi-financial-id";
  private static final String HEADER_AUTHORIZATION = "Authorization";

  private RestTemplate restTemplate;

  private static IKonsentRestClient _instance;

  private IKonsentRestClient() {
    restTemplate = new RestTemplate(clientHttpRequestFactory());;
  }

  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  private ClientHttpRequestFactory clientHttpRequestFactory() {
    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
    factory.setReadTimeout(Integer.parseInt(System.getProperty("read.timeout", "20000")));
    factory.setConnectTimeout(Integer.parseInt(System.getProperty("connect.timeout", "10000")));
    factory.setBufferRequestBody(true);
    return factory;
  }

  public static synchronized IKonsentRestClient getInstance() {
    if (null == _instance) {
      _instance = new IKonsentRestClient();
    }
    return _instance;
  }

  public <T> ResponseEntity<T> postForEntity(String url, String siteId, Object request,
      Class<T> responseType, Object... uriVariables) {
    return restTemplate.postForEntity(url, request, responseType, uriVariables);
  }

  public <T> ResponseEntity<T> getForEntity(String url, String financialId, String authorization,
      Class<T> responseType) {
    restTemplate.setInterceptors(
        Arrays.asList(new HeaderRequestInterceptor(HEADER_FINANCIAL_ID, financialId),
            new HeaderRequestInterceptor(HEADER_AUTHORIZATION,
                String.format("Bearer %s", authorization))));
    
    return restTemplate.getForEntity(url, responseType);
  }

}
