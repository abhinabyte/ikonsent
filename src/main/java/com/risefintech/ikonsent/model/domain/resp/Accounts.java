
package com.risefintech.ikonsent.model.domain.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Accounts {

  @JsonProperty("Data")
  private Data data;

}
