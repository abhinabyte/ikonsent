/**
 * 
 */
package com.risefintech.ikonsent.model.domain.resp;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Balance {

  @JsonProperty("AccountId")
  private String accountId;

  @JsonProperty("Amount")
  private Amount amount;

  @JsonProperty("CreditDebitIndicator")
  private String creditDebitIndicator;

  @JsonProperty("Type")
  private String type;

  @JsonProperty("DateTime")
  private String dateTime;

  @JsonProperty("CreditLine")
  private List<CreditLine> creditLine = null;

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class Amount {

    @JsonProperty("Amount")
    private String amount;

    @JsonProperty("Currency")
    private String currency;

  }

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class CreditLine {

    @JsonProperty("Included")
    private Boolean included;

    @JsonProperty("Amount")
    private Amount amount;

    @JsonProperty("Type")
    private String type;

  }

}
