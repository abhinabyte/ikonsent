package com.risefintech.ikonsent.model.domain.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StandingOrder {

  @JsonProperty("AccountId")
  private String accountId;

  @JsonProperty("StandingOrderId")
  private String standingOrderId;

  @JsonProperty("Frequency")
  private String frequency;

  @JsonProperty("Reference")
  private String reference;

  @JsonProperty("FirstPaymentDateTime")
  private String firstPaymentDateTime;

  @JsonProperty("FirstPaymentAmount")
  private FirstPaymentAmount firstPaymentAmount;

  @JsonProperty("NextPaymentDateTime")
  private String nextPaymentDateTime;

  @JsonProperty("NextPaymentAmount")
  private NextPaymentAmount nextPaymentAmount;

  @JsonProperty("FinalPaymentDateTime")
  private String finalPaymentDateTime;

  @JsonProperty("FinalPaymentAmount")
  private FinalPaymentAmount finalPaymentAmount;

  @JsonProperty("CreditorAccount")
  private CreditorAccount creditorAccount;

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class FirstPaymentAmount {

    @JsonProperty("Amount")
    private String amount;

    @JsonProperty("Currency")
    private String currency;

  }

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class NextPaymentAmount {

    @JsonProperty("Amount")
    private String amount;

    @JsonProperty("Currency")
    private String currency;

  }

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class FinalPaymentAmount {

    @JsonProperty("Amount")
    private String amount;

    @JsonProperty("Currency")
    private String currency;

  }

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class CreditorAccount {

    @JsonProperty("SchemeName")
    private String schemeName;

    @JsonProperty("Identification")
    private String identification;

    @JsonProperty("Name")
    private String name;

  }

}
