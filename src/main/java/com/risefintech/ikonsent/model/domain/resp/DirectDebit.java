/**
 * 
 */
package com.risefintech.ikonsent.model.domain.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DirectDebit {

  @JsonProperty("AccountId")
  private String accountId;

  @JsonProperty("DirectDebitId")
  private String directDebitId;

  @JsonProperty("MandateIdentification")
  private String mandateIdentification;

  @JsonProperty("DirectDebitStatusCode")
  private String directDebitStatusCode;

  @JsonProperty("Name")
  private String name;

  @JsonProperty("PreviousPaymentDateTime")
  private String previousPaymentDateTime;

  @JsonProperty("PreviousPaymentAmount")
  private PreviousPaymentAmount previousPaymentAmount;

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class PreviousPaymentAmount {

    @JsonProperty("Amount")
    private String amount;

    @JsonProperty("Currency")
    private String currency;

  }

}
