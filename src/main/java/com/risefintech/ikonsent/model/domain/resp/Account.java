
package com.risefintech.ikonsent.model.domain.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {

  @JsonProperty("AccountId")
  private String accountId;

  @JsonProperty("Currency")
  private String currency;

  @JsonProperty("AccountType")
  private String accountType;

  @JsonProperty("AccountSubType")
  private String accountSubType;

  @JsonProperty("Nickname")
  private String nickname;

  @JsonProperty("SchemeName")
  private String schemeName;

  @JsonProperty("Identification")
  private String identification;

  @JsonProperty("Name")
  private String name;

  @JsonProperty("SecondaryIdentification")
  private String secondaryIdentification;

  @JsonProperty("Account")
  private Account accountDetail;

}
