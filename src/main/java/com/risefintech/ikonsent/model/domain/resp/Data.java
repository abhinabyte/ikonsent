
package com.risefintech.ikonsent.model.domain.resp;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

  @JsonProperty("Account")
  private List<Account> accounts;

  @JsonProperty("Product")
  private List<Product> products;

  @JsonProperty("Balance")
  private List<Balance> balances;

  @JsonProperty("Transaction")
  private List<Transaction> transactions;

  @JsonProperty("DirectDebit")
  private List<DirectDebit> directDebits;

  @JsonProperty("StandingOrder")
  private List<StandingOrder> standingOrders;

  @JsonProperty("Beneficiary")
  public List<Beneficiary> beneficiaries;

}
