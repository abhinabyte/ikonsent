
package com.risefintech.ikonsent.model.domain.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 
 * @author abhinab
 *
 */
@Data
public class GenericRequest {

  @JsonProperty("psp_id")
  private String pspId;

  private String authorization;

}
