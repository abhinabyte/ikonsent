/**
 * 
 */
package com.risefintech.ikonsent.model.domain.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {

  @JsonProperty("AccountId")
  private String accountId;

  @JsonProperty("TransactionId")
  private String transactionId;

  @JsonProperty("TransactionReference")
  private String transactionReference;

  @JsonProperty("Amount")
  private Balance.Amount amount;

  @JsonProperty("CreditDebitIndicator")
  private String creditDebitIndicator;

  @JsonProperty("Status")
  private String status;

  @JsonProperty("BookingDateTime")
  private String bookingDateTime;

  @JsonProperty("ValueDateTime")
  private String valueDateTime;

  @JsonProperty("TransactionInformation")
  private String transactionInformation;

  @JsonProperty("BankTransactionCode")
  private BankTransactionCode bankTransactionCode;

  @JsonProperty("ProprietaryBankTransactionCode")
  private ProprietaryBankTransactionCode proprietaryBankTransactionCode;

  @JsonProperty("Balance")
  private Balance balance;

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class BankTransactionCode {

    @JsonProperty("Code")
    private String code;

    @JsonProperty("SubCode")
    private String subCode;

  }

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class ProprietaryBankTransactionCode {

    @JsonProperty("Code")
    private String code;

    @JsonProperty("Issuer")
    private String issuer;

  }

}
