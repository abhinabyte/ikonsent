/**
 * 
 */
package com.risefintech.ikonsent.model.domain.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

  @JsonProperty("AccountId")
  private String accountId;

  @JsonProperty("ProductIdentifier")
  private String productIdentifier;

  @JsonProperty("ProductType")
  private String productType;

  @JsonProperty("ProductName")
  private String productName;

}
