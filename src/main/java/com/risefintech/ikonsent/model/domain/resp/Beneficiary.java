/**
 * 
 */
package com.risefintech.ikonsent.model.domain.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author abhinab
 *
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Beneficiary {

  @JsonProperty("AccountId")
  private String accountId;

  @JsonProperty("BeneficiaryId")
  private String beneficiaryId;

  @JsonProperty("Reference")
  private String reference;

  @JsonProperty("CreditorAccount")
  private CreditorAccount creditorAccount;

  @lombok.Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public class CreditorAccount {

    @JsonProperty("SchemeName")
    private String schemeName;

    @JsonProperty("Identification")
    private String identification;

    @JsonProperty("Name")
    private String name;

  }

}
